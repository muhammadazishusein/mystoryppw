from django.urls import path
from . import views
from django.conf.urls import url

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name = 'index'),
    path('jadwal/', views.jadwal, name = 'jadwal'),
    path('delete/', views.delete, name = 'delete')
]
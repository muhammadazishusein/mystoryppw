from django.db import models
from django.utils import timezone
import datetime
# from django.forms import ModelForm

# Create your models here.
class Schedule(models.Model):
    name = models.CharField(max_length = 25)
    category = models.CharField(max_length = 25)
    place = models.CharField(max_length = 25)
    date = models.DateTimeField()

    # def __str__(self):
    #     return self.name
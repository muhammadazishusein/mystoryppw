from django import forms
from .models import Schedule
from django.forms import ModelForm
import datetime

CATEGORY_LIST = [
    ('rapat', 'Rapat'),
    ('kelas', 'Kelas'),
    ('belajar', 'Belajar'),
    ('acara', 'Acara'),
    ('lain-lain', 'Lain-lain'),
    ('deadline', 'Deadline'),
]

class ScheduleForm(ModelForm):
    category = forms.CharField(max_length = 25, label='Kategori jadwal', widget=forms.Select(choices=CATEGORY_LIST))
    class Meta:
        model = Schedule

        fields = ["name", 'category', 'place', 'date']

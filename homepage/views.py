from django.shortcuts import render, redirect
from .forms import ScheduleForm
from .models import Schedule
from django.http import HttpResponse
from django.http import HttpResponseRedirect
import datetime

# Create your views here.
def index(request):
    return render(request, 'index.html')

def jadwal(request):
    if request.method == 'GET':
        form = ScheduleForm()
    elif request.method == 'POST':
        form = ScheduleForm(request.POST)
        print(request.POST)
        if form.is_valid():
            new_schedule = Schedule(
                name = form.data['name'],
                category = form.data['category'],
                place = form.data['place'],
                date = form.data['date'],
            )

            new_schedule.save()
            form = ScheduleForm()
        return HttpResponseRedirect('/jadwal/')
    
    schedules = Schedule.objects.all()
    context = {
        'form': form,
        'schedules': schedules,
    }
    return render(request, 'jadwal.html', context)

def delete(request):
    Schedule.objects.filter(id = int(request.GET.get('arg', ''))).delete()
    return HttpResponseRedirect('/jadwal/')